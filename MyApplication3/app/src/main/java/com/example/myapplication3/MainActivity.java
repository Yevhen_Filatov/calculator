package com.example.myapplication3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView result;
    EditText number1,number2,operation;
    Button add, substract,multiplay,divide,calc;

    double result_num;
    double num11,num22;
    String opp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result=(TextView)findViewById(R.id.result);
        number1=(EditText)findViewById(R.id.num1);
        number2=(EditText)findViewById(R.id.num2);
        operation=(EditText)findViewById(R.id.operation);

        calc=(Button)findViewById(R.id.calc);



        calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                num11=Double.parseDouble(number1.getText().toString());
                num22=Double.parseDouble(number2.getText().toString());
                opp=(operation.getText().toString());
                switch (opp) {
                    case "+":
                        result_num=num11+num22;
                        break;
                    case "-":
                        result_num=num11-num22;
                        break;
                    case "*":
                        result_num=num11*num22;;
                        break;
                    case "/":

                            result_num=num11/num22;

                        break;


                }

                result.setText(String.valueOf(result_num));
            }
        });



    }
}