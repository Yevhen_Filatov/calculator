package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private TextField num1Field;

    @FXML
    private TextField num2Field;
    @FXML
    private TextField num3Field;

    @FXML
    private TextField resultField;
    private double num1;
    private double num2;
    private double result;
    private String operator;
    private model model =new model();
    @FXML
    void calcClick(ActionEvent event) {
    num1=Double.parseDouble(num1Field.getText());
    num2=Double.parseDouble(num2Field.getText());
    operator=(num3Field.getText());
    result=model.calculate(num1,num2,operator);
    resultField.setText(String.valueOf(result));
    }

}
